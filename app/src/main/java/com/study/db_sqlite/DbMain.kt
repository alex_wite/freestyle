/*
package com.study.db_sqlite

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.*
import com.study.R
import com.study.anim.Anim
import com.study.anim.JumpAnimation
import com.study.calculator.Calculator
import com.study.enter.Enter
import com.study.laba3.Laba3Main
import kotlinx.android.synthetic.main.activity_calc.*
import kotlinx.android.synthetic.main.activity_main_db.*
import kotlinx.android.synthetic.main.activity_note.*
import java.lang.Exception

class DbMain : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout

    private var listNotes = ArrayList<Note>()
    var id = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_db)


//        listNotes.add(Note(1, "JavaSampleApproach", "Java technology, Spring Framework - approach to Java by Sample."))
//        listNotes.add(Note(2, "Kotlin Android Tutorial", "Create tutorial for people to learn Kotlin Android. Kotlin is now an official language on Android. It's expressive, concise, and powerful. Best of all, it's interoperable with our existing Android languages and runtime."))
//        listNotes.add(Note(3, "Android Studio", "Android Studio 3.0 provides helpful tools to help you start using Kotlin. Convert entire Java files or convert code snippets on the fly when you paste Java code into a Kotlin file."))
//        listNotes.add(Note(4, "Java Android Tutorial", "Create tutorial for people to learn Java Android. Learn Java in a greatly improved learning environment with more lessons, real practice opportunity, and community support."))
//        listNotes.add(Note(5, "Spring Boot Tutorial", "Spring Boot help build stand-alone, production Spring Applications easily, less configuration then rapidly start new projects."))

        loadQueryAll()
        
        lvNote.onItemClickListener = AdapterView.OnItemClickListener {adapterView, view, position, id ->
            Toast.makeText(this, "Ты тыкнул на " + listNotes[position].title, Toast.LENGTH_SHORT).show()
        }

        drawerLayout = findViewById(R.id.drawer_layout)

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true

            when (menuItem.itemId) {
                R.id.menuToCalc -> {
                    val i = Intent(this, Calculator::class.java)
                    startActivity(i)
                }
                R.id.menuToSecAct -> {
                    val i = Intent(this, Enter::class.java)
                    startActivity(i)
                }
*/
/*                R.id.menuToDb -> {
                    val i = Intent(this, DbMain::class.java)
                    startActivity(i)
                }*//*

                R.id.menuToAnim -> {
                    val i = Intent(this, Anim::class.java)
                    startActivity(i)
                }
                R.id.menuToLaba3 -> {
                    val i = Intent(this, Laba3Main::class.java)
                    startActivity(i)
                }
            }
            // close drawer when item is tapped
            drawerLayout.closeDrawers()

            true
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if ( item != null ) {
            when (item.itemId) {
                R.id.addNote -> {
                    var intent = Intent(this, NoteActivity::class.java)
                    startActivity(intent)
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        loadQueryAll()

        if (listNotes.isNotEmpty()){
            list_empty.visibility = View.GONE
        } else {
            list_empty.visibility = View.VISIBLE
//            lvNote.visibility = View.GONE
        }
    }

    fun loadQueryAll(){
        var dbManager = NoteDbManager(this)
        val cursor = dbManager.queryAll()

        listNotes.clear()
        if (cursor.moveToFirst()){

            do {
                val id = cursor.getInt(cursor.getColumnIndex("Id"))
                val title = cursor.getString(cursor.getColumnIndex("Title"))
                val content = cursor.getString(cursor.getColumnIndex("Content"))

                listNotes.add(Note(id, title, content))
            } while (cursor.moveToNext())
        }

        var notesAdapter = NotesAdapter(this, listNotes)
        lvNote.adapter = notesAdapter
    }

    inner class NotesAdapter : BaseAdapter {
        private var notesList = ArrayList<Note>()
        private var context: Context? = null

        constructor(context: Context, notesList: ArrayList<Note>) : super(){
            this.notesList = notesList
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null){
                view = layoutInflater.inflate(R.layout.note, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
                Log.i("JSA", "set Tag for ViewHolder, position: " + position)
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }

            var mNote = notesList[position]

            vh.tvTitle.text = mNote.title
            vh.tvContent.text = mNote.content

            vh.ivEdit.setOnClickListener {
                updateNote(mNote)
            }

            vh.ivDelete.setOnClickListener {
                var dbManager = NoteDbManager(this.context!!)
                val selectionArgs = arrayOf(mNote.id.toString())
                dbManager.delete("Id=?", selectionArgs)
                loadQueryAll()
            }

            return view
        }

        override fun getItem(position: Int): Any {
            return notesList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return notesList.size
        }

        private fun updateNote(note: Note){
            val intent = Intent(this.context, NoteActivity::class.java)
            intent.putExtra("MainActId", note.id)
            intent.putExtra("MainActTitle", note.title)
            intent.putExtra("MainActContent", note.content)
            startActivity(intent)
        }

        private inner class ViewHolder(view: View?) {
            val tvTitle: TextView
            val tvContent: TextView
            val ivEdit: ImageView
            val ivDelete: ImageView

            init{
                this.tvTitle = view?.findViewById<TextView>(R.id.tvTitle) as TextView
                this.tvContent = view.findViewById<TextView>(R.id.tvContent) as TextView
                this.ivEdit = view.findViewById<ImageView>(R.id.ivEdit) as ImageView
                this.ivDelete = view.findViewById<ImageView>(R.id.ivDelete) as ImageView
            }
        }
    }
}*/
