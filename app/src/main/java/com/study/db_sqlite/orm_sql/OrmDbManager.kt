/*
package com.study.db_sqlite.orm_sql

import android.arch.persistence.room.*

class OrmDbManager {
    @Entity(tableName = "contacts",
        indices = [Index("id")])
    data class RoomContact(
        @PrimaryKey(autoGenerate = true)
        val id: Int?,
        val name: String,
        val phone: String
    )
}

@Dao
interface DbDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContact(room: OrmDbManager.RoomContact)

    @Update
    fun updateContact(room: OrmDbManager.RoomContact)

    @Delete
    fun deleteContact(room: OrmDbManager.RoomContact)

    @Query("SELECT * FROM contacts")
    fun getContacts(): List<OrmDbManager.RoomContact>
}*/
