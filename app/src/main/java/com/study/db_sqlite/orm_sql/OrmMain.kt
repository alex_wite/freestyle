/*
package com.study.db_sqlite.orm_sql

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.study.R
import com.study.anim.Anim
import com.study.calculator.Calculator
import com.study.db_sqlite.DbMain
import com.study.enter.Enter
import com.study.laba3.Laba3Main
import kotlinx.android.synthetic.main.activity_main_db.*

class OrmMain(private var contactList: ArrayList<OrmDbManager.RoomContact>, private val orm: DbDao) : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_db)

        drawerLayout = findViewById(R.id.drawer_layout)

        orm.getContacts()

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true

            when (menuItem.itemId) {
                R.id.menuToCalc -> {
                    val i = Intent(this, Calculator::class.java)
                    startActivity(i)
                }
                R.id.menuToSecAct -> {
                    val i = Intent(this, Enter::class.java)
                    startActivity(i)
                }
                R.id.menuToDb -> {
                    val i = Intent(this, DbMain::class.java)
                    startActivity(i)
                }
                R.id.menuToAnim -> {
                    val i = Intent(this, Anim::class.java)
                    startActivity(i)
                }
                R.id.menuToLaba3 -> {
                    val i = Intent(this, Laba3Main::class.java)
                    startActivity(i)
                }
            }
            // close drawer when item is tapped
            drawerLayout.closeDrawers()

            true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if ( item != null ) {
            when (item.itemId) {
                R.id.addNote -> {
                    var intent = Intent(this, OrmNote::class.java)
                    startActivity(intent)
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()

        orm.getContacts()

        if (contactList.isNotEmpty()){
            list_empty.visibility = View.GONE
        } else {
            list_empty.visibility = View.VISIBLE
        }
    }

    inner class ContactsAdapter : BaseAdapter {
        private var contactsList = ArrayList<OrmDbManager.RoomContact>()
        private var context: Context? = null

        constructor(context: Context, contactsList: ArrayList<OrmDbManager.RoomContact>) : super(){
            this.contactsList = contactsList
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null){
                view = layoutInflater.inflate(R.layout.note, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
                Log.i("JSA", "set Tag for ViewHolder, position: " + position)
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }

            var mContact = contactsList[position]

            vh.tvTitle.text = mContact.name
            vh.tvContent.text = mContact.phone

            vh.ivEdit.setOnClickListener {
//                updateNote(mContact)
                orm.updateContact(mContact)
            }

            vh.ivDelete.setOnClickListener {
//                var dbManager = OrmDbManager(this.context!!)
//                val selectionArgs = arrayOf(mContact.id.toString())
                orm.deleteContact(mContact)
            }

            return view
        }

        override fun getItem(position: Int): Any {
            return contactsList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return contactsList.size
        }
*/
/*
        private fun updateNote(note: Note){
            val intent = Intent(this.context, NoteActivity::class.java)
            intent.putExtra("MainActId", note.id)
            intent.putExtra("MainActTitle", note.title)
            intent.putExtra("MainActContent", note.content)
            startActivity(intent)
        }*//*


        private inner class ViewHolder(view: View?) {
            val tvTitle: TextView
            val tvContent: TextView
            val ivEdit: ImageView
            val ivDelete: ImageView

            init{
                this.tvTitle = view?.findViewById<TextView>(R.id.tvTitle) as TextView
                this.tvContent = view.findViewById<TextView>(R.id.tvContent) as TextView
                this.ivEdit = view.findViewById<ImageView>(R.id.ivEdit) as ImageView
                this.ivDelete = view.findViewById<ImageView>(R.id.ivDelete) as ImageView
            }
        }
    }
}*/
