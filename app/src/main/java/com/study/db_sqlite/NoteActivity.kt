/*
package com.study.db_sqlite

import android.content.ContentValues
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.study.R
import kotlinx.android.synthetic.main.activity_note.*
import java.lang.Exception

class NoteActivity : AppCompatActivity() {

    var id = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)

        try {
            var bundle: Bundle = intent.extras
            id = bundle.getInt("MainActId", 0)
            if (id != 0){
                editTitle.setText(bundle.getString("MainActTitle"))
                editContent.setText(bundle.getString("MainActContent"))
            }
        } catch (ex: Exception){
        }

        btnAdd.setOnClickListener{
            var dbManager = NoteDbManager(this)

            var values = ContentValues()
            values.put("Title", editTitle.text.toString())
            values.put("Content", editContent.text.toString())

            if (id == 0) {
                val mID = dbManager.insert(values)

                if (mID > 0) {
                    Toast.makeText(this, "Успешно добавлено!", Toast.LENGTH_LONG).show()
                    finish()
                } else {
                    Toast.makeText(this, "Ошибка добавления :-(", Toast.LENGTH_LONG).show()
                }
            } else {
                var selectionArs = arrayOf(id.toString())
                val mID = dbManager.update(values, "Id=?", selectionArs)

                if (mID > 0) {
                    Toast.makeText(this, "Успешно добавлено!", Toast.LENGTH_LONG).show()
                    finish()
                } else {
                    Toast.makeText(this, "Ошибка добавления :-(", Toast.LENGTH_LONG).show()
                }
            }
        }

        btnCancel.setOnClickListener{
            Toast.makeText(this, getString(R.string.note_cancle), Toast.LENGTH_LONG).show()
            finish()
        }
    }
}*/
