package com.study.enter

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.study.R
import com.study.anim.Anim
import com.study.anim.JumpAnimation
import com.study.calculator.Calculator
//import com.study.db_sqlite.DbMain
import com.study.laba3.Laba3Main
import kotlinx.android.synthetic.main.activity_hello.*
import kotlinx.android.synthetic.main.anim_activity.*

class Enter : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout

    var listUsers = ArrayList<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        listUsers.add(User(1, "admin", 123, "Админушка"))
        listUsers.add(User(2, "admin2", 456, "Валентин Будэйко"))
        listUsers.add(User(3, "admin3", 789, "Степунчик"))
        listUsers.add(User(4, "admin4", 147, "Димоноид"))


        butEnter.setOnClickListener {

            if (password.text.isEmpty()){
                Toast.makeText(this, getString(R.string.pass_warning), Toast.LENGTH_SHORT).show()
            }

            val neededUser = listUsers.find {
                val pas = if (!password.text.isEmpty()) {
                    password.text.toString().toInt()
                } else {
                    Toast.makeText(this, getString(R.string.pass_warning), Toast.LENGTH_SHORT).show()
                }
                it.psswrd == pas &&
                        it.lgn == login.text.toString()
            }

            val idU = neededUser?.idUser
            val userN = neededUser?.userName

            if (idU == null){
                Toast.makeText(this, getString(R.string.warning_auth), Toast.LENGTH_SHORT).show()
            } else {
                val intent = Intent(this, SecondActivity::class.java)
                intent.putExtra(USER_NAME, userN)
                startActivity(intent)
            }
        }

        drawerLayout = findViewById(R.id.drawer_layout)

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true

            when (menuItem.itemId) {
                R.id.menuToCalc -> {
                    val i = Intent(this, Calculator::class.java)
                    startActivity(i)
                }
                R.id.menuToSecAct -> {
                    val i = Intent(this, Enter::class.java)
                    startActivity(i)
                }
/*                R.id.menuToDb -> {
                    val i = Intent(this, DbMain::class.java)
                    startActivity(i)
                }*/
                R.id.menuToAnim -> {
                    val i = Intent(this, Anim::class.java)
                    startActivity(i)
                }
                R.id.menuToLaba3 -> {
                    val i = Intent(this, Laba3Main::class.java)
                    startActivity(i)
                }
            }
            // close drawer when item is tapped
            drawerLayout.closeDrawers()

            true
        }

        val plAn = JumpAnimation()
        plAn.PlayAnim(jumpAnimEn)

        jumpAnimEn.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }
}