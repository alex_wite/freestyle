package com.study.enter

class User {
    var idUser: Int? = null
    var lgn: String? = null
    var psswrd: Int? = null
    var userName: String? = null

    constructor(idUser: Int, lgn: String, psswrd: Int, userName: String){
        this.idUser = idUser
        this.lgn = lgn
        this.psswrd = psswrd
        this.userName = userName
    }
}