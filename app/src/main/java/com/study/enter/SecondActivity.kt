package com.study.enter

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.study.MainActivity
import com.study.R
import kotlinx.android.synthetic.main.second_activity.*

class SecondActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)

        val nameIntent = intent.getStringExtra(USER_NAME)

        val result = "Здравствуй, $nameIntent! Как дела?"
        resultAuth.text = result

        backToMain.setOnClickListener {
            val intent = Intent(this, Enter::class.java)
            startActivity(intent)
            finish()
        }

    }
}