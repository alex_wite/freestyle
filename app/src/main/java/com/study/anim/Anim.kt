package com.study.anim

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import com.study.MainActivity
import com.study.R
import com.study.calculator.Calculator
//import com.study.db_sqlite.DbMain
import com.study.enter.Enter
import com.study.laba3.Laba3Main
import kotlinx.android.synthetic.main.anim_activity.*

class Anim : AppCompatActivity() {

    private val plAn = JumpAnimation()

    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.anim_activity)

        drawerLayout = findViewById(R.id.drawer_layout)

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true

            when (menuItem.itemId) {
                R.id.menuToCalc -> {
                    val i = Intent(this, Calculator::class.java)
                    startActivity(i)
                }
                R.id.menuToSecAct -> {
                    val i = Intent(this, Enter::class.java)
                    startActivity(i)
                }
/*                R.id.menuToDb -> {
                    val i = Intent(this, DbMain::class.java)
                    startActivity(i)
                }*/
                R.id.menuToAnim -> {
                    val i = Intent(this, Anim::class.java)
                    startActivity(i)
                }
                R.id.menuToLaba3 -> {
                    val i = Intent(this, Laba3Main::class.java)
                    startActivity(i)
                }
            }
            // close drawer when item is tapped
            drawerLayout.closeDrawers()

            true
        }

        plAn.PlayAnim(jumpAnim)


        for (i in 1..2) {
            plAn.toRightUpQ(roundBtn)
        }

        roundBtn.setOnClickListener {
            plAn.toRightUpQ(roundBtn)
        }


        backMain.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        jumpAnim.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }
}


