package com.study.anim

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.support.v4.view.ViewCompat
import android.view.View
import android.view.animation.*

class JumpAnimation {

    fun jumpUp(view: View): Animator {
        val animator = ValueAnimator.ofFloat(100f)

        animator.addUpdateListener {
            val value = it.animatedValue as Float
            view.translationY = -value
            view.translationZ = value
        }

        animator.repeatMode = ValueAnimator.REVERSE
        animator.repeatCount = 1

        animator.duration = 750L
        animator.interpolator = AnticipateInterpolator()
        animator.startDelay = 3000L
//            animator.startDelay = 500L
        animator.start()
        return animator
    }

    fun jumpRight(view: View): Animator {
        val animator = ValueAnimator.ofFloat(150f)

        animator.addUpdateListener {
            val value = it.animatedValue as Float
            view.translationX = value
        }

        animator.repeatMode = ValueAnimator.REVERSE
        animator.repeatCount = 1

        animator.duration = 750L
        animator.interpolator = AnticipateInterpolator()
        animator.startDelay = 6750L
//            animator.startDelay = 1500L
        animator.start()
        return animator
    }

    fun PlayAnim(view: View): Animator? {

        val animatorSet = AnimatorSet()

        animatorSet.play(jumpUp(view)).after(jumpRight(view))
        return animatorSet
    }

    /*val animator = ValueAnimator.ofFloat(0f, 100f)
    animator.duration = 1000
    animator.start()

    animator.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
        override fun onAnimationUpdate(animation: ValueAnimator){
            val  animationValue = animation.animatedValue as Float
            jumpAnim.translationX = animationValue
        }
    })*/

    /*val objectAnimator = ObjectAnimator.ofFloat(jumpAnim, "translationX", 100f)
    objectAnimator.duration = 1000
    objectAnimator.start()*/

    /*val bouncer = AnimatorSet()

    bouncer.play(bounceAnim).before(squashAnim1)
    bouncer.play(squashAnim1).before(squashAnim2)

    val fadeAnim = ObjectAnimator.ofFloat(newBall, "alpha", 1f, 0f)
    fadeAnim.duration = 250
    val animatorSet = AnimatorSet()
    animatorSet.play(bouncer).before(fadeAnim)
    animatorSet.start()*/

     /*ViewCompat.animate(jumpAnim)
         .translationX(200f)
         .translationY(-400f)
         .translationZ(100f)
         .scaleX(2f)
         .scaleY(2f)
         .setDuration(3000)
         .setInterpolator(AccelerateInterpolator())
         .setStartDelay(1000)
         .withEndAction {
             ViewCompat.animate(jumpAnim)
         }
         .setListener(object : Animator.AnimatorListener {
             override fun onAnimationRepeat(animation: Animator) {}
             override fun onAnimationEnd(animation: Animator) {}
             override fun onAnimationCancel(animation: Animator) {}
             override fun onAnimationStart(animation: Animator) {}
         })
*/
    fun roundAnim(view: View): Animator{
        val animatorSet = AnimatorSet()


        return animatorSet
    }

    fun translationUp (view: View): Animator{
        val animator = ValueAnimator.ofFloat(300f)

        animator.addUpdateListener {
            val value = it.animatedValue as Float
            view.translationY = -value
        }

        animator.duration = 1500L
        animator.startDelay = 500L
        animator.interpolator = DecelerateInterpolator()

        animator.start()

        return animator
    }

    fun translationRight(view: View): Animator{
        val animator = ValueAnimator.ofFloat(300f)

        animator.addUpdateListener {
            val value = it.animatedValue as Float
            view.translationX = value
        }

        animator.duration = 1500L
        animator.startDelay = 1700L
        animator.interpolator = DecelerateInterpolator()

        animator.start()

        return animator
    }

    fun translationBottom(view: View): Animator{
        val animator = ValueAnimator.ofFloat(300f)

        animator.addUpdateListener {
            val value = it.animatedValue as Float
            view.translationY = value
        }

        animator.duration = 1150L
        animator.startDelay = 3550L
        animator.interpolator = DecelerateInterpolator()

        animator.start()

        return animator
    }

    fun toRightUpQ(view: View) {
/*
        val animatorSet = AnimatorSet()

        animatorSet.play(translationRight(view)).before(translationBottom(view)).after(translationUp(view))
        return animatorSet*/
        ViewCompat.animate(view)
            .translationX(200f)
            .translationY(-400f)
            .translationZ(100f)
            .scaleX(2f)
            .scaleY(2f)
            .setDuration(3000)
            .setInterpolator(AnticipateInterpolator())
            .setStartDelay(1000)
            .withEndAction {
                ViewCompat.animate(view)
                .translationX(-200f)
                .translationY(400f)
                .translationZ(-100f)
                .scaleX(-2f)
                .scaleY(-2f)
                .rotationX(180f)
            }
    }
}