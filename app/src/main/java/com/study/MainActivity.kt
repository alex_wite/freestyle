package com.study

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import com.study.anim.Anim
import com.study.anim.JumpAnimation
import com.study.calculator.Calculator
//import com.study.db_sqlite.DbMain
import com.study.enter.Enter
import com.study.laba3.Laba3Main
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.anim_activity.*

class MainActivity : AppCompatActivity(){

    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonToCalc.setOnClickListener{
            var intent = Intent(this, Calculator::class.java)
            startActivity(intent)
        }

        buttonToSecAct.setOnClickListener{
            var intent = Intent(this, Enter::class.java)
            startActivity(intent)
        }

/*        buttonToDb.setOnClickListener {
            var intent = Intent(this, DbMain::class.java)
            startActivity(intent)
        }*/

        buttonToAnim.setOnClickListener {
            var intent = Intent(this, Anim::class.java)
            startActivity(intent)
        }

        buttonToLaba3.setOnClickListener {
            var intent = Intent(this, Laba3Main::class.java)
            startActivity(intent)
        }

        drawerLayout = findViewById(R.id.drawer_layout)

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true

            when (menuItem.itemId) {
                R.id.menuToCalc -> {
                    val i = Intent(this, Calculator::class.java)
                    startActivity(i)
                }
                R.id.menuToSecAct -> {
                    val i = Intent(this, Enter::class.java)
                    startActivity(i)
                }
/*                R.id.menuToDb -> {
                    val i = Intent(this, DbMain::class.java)
                    startActivity(i)
                }*/
                R.id.menuToAnim -> {
                    val i = Intent(this, Anim::class.java)
                    startActivity(i)
                }
                R.id.menuToLaba3 -> {
                    val i = Intent(this, Laba3Main::class.java)
                    startActivity(i)
                }
            }
            // close drawer when item is tapped
            drawerLayout.closeDrawers()

            true
        }

        val plAn = JumpAnimation()
        plAn.PlayAnim(jumpAnimMain)

        jumpAnimMain.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }
}