package com.study.laba3

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.study.R
import com.study.anim.Anim
import com.study.anim.JumpAnimation
import com.study.calculator.Calculator
//import com.study.db_sqlite.DbMain
import com.study.enter.*
import kotlinx.android.synthetic.main.activity_calc.*
import kotlinx.android.synthetic.main.laba3_main.*

class Laba3Main : AppCompatActivity()  {

    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.laba3_main)

        button_laba3.setOnClickListener {
            val value = editText.text.toString()

            if (value.isBlank()){
                Toast.makeText(this, getString(R.string.warning_2), Toast.LENGTH_LONG).show()
            } else {
                val int = Intent(this, Laba3Second::class.java)
                int.putExtra(EDIT_VALUE, value)
//                startActivity(int)
                startActivityForResult(int, REQUST_CODE)
            }

        }

        drawerLayout = findViewById(R.id.drawer_layout)

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true

            when (menuItem.itemId) {
                R.id.menuToCalc -> {
                    val i = Intent(this, Calculator::class.java)
                    startActivity(i)
                }
                R.id.menuToSecAct -> {
                    val i = Intent(this, Enter::class.java)
                    startActivity(i)
                }
/*                R.id.menuToDb -> {
                    val i = Intent(this, DbMain::class.java)
                    startActivity(i)
                }*/
                R.id.menuToAnim -> {
                    val i = Intent(this, Anim::class.java)
                    startActivity(i)
                }
                R.id.menuToLaba3 -> {
                    val i = Intent(this, Laba3Main::class.java)
                    startActivity(i)
                }
            }
            // close drawer when item is tapped
            drawerLayout.closeDrawers()

            true
        }

        val plAn = JumpAnimation()
        plAn.PlayAnim(jumpAnim)

        jumpAnim.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK){

            if (requestCode == REQUST_CODE) {

                if (data != null){
                    val valueFromSecond = data.getStringExtra(EDIT_VALUE)

                    resultLaba3.text = valueFromSecond
                }
            }
        }
    }
}