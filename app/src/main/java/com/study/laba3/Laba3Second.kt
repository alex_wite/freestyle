package com.study.laba3

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.study.R
import com.study.enter.*
import kotlinx.android.synthetic.main.laba3_second.*

class Laba3Second : AppCompatActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.laba3_second)

        parseIntent()

        button_laba3.setOnClickListener {
            val value = editText.text.toString()

            val int = Intent()

            if (value.isBlank()){
                int.putExtra(EDIT_VALUE, getString(R.string.empty))
            } else {
                int.putExtra(EDIT_VALUE, value)
            }

            setResult(Activity.RESULT_OK, int)
            finish()
        }
    }

    fun parseIntent(){
        val valueFromMain = intent.getStringExtra(EDIT_VALUE)

        resultLaba3.text = valueFromMain
    }
}