package com.study.calculator

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import com.study.R
import com.study.anim.Anim
import com.study.anim.JumpAnimation
//import com.study.db_sqlite.DbMain
import com.study.enter.Enter
import com.study.laba3.Laba3Main
import kotlinx.android.synthetic.main.activity_calc.*

class Calculator : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calc)

        val spinnerArrayAdapter =
            ArrayAdapter.createFromResource(this, R.array.choose, android.R.layout.simple_spinner_item)

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        my_spinner.adapter = spinnerArrayAdapter

        calcButton.setOnClickListener {

            if (editA.text.isEmpty() || editB.text.isEmpty()){
                if (editA.text.isEmpty() && editB.text.isEmpty()){
                    Toast.makeText(this, getString(R.string.warning_2), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, getString(R.string.warning_1), Toast.LENGTH_SHORT).show()
                }
            } else {
                val textA = editA.text.toString().toInt()
                val textB = editB.text.toString().toInt()

                val resultIt = if (my_spinner.selectedItem.toString() == "Сумма"){
                    sum(textA, textB)
                } else {
                    if (my_spinner.selectedItem.toString() == "Разность") {
                        raznost(textA, textB)
                    } else{
                        if (my_spinner.selectedItem.toString() == "Произведение") {
                            proizvedenie(textA, textB)
                        } else {
                            val textC = parse(editA)
                            val textD = parse(editB)
                            if (textD == 0.0){
                                getString(R.string.infinity)//"∞"
                            } else {
                                delenie(textC, textD)
                            }
                        }
                    }
                }
                val resultItText = "${resources.getString(R.string.result)}  =  $resultIt"
                resultView.text = resultItText
            }
        }

        drawerLayout = findViewById(R.id.drawer_layout)

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true

            when (menuItem.itemId) {
                R.id.menuToCalc -> {
                    val i = Intent(this, Calculator::class.java)
                    startActivity(i)
                }
                R.id.menuToSecAct -> {
                    val i = Intent(this, Enter::class.java)
                    startActivity(i)
                }
/*                R.id.menuToDb -> {
                    val i = Intent(this, DbMain::class.java)
                    startActivity(i)
                }*/
                R.id.menuToAnim -> {
                    val i = Intent(this, Anim::class.java)
                    startActivity(i)
                }
                R.id.menuToLaba3 -> {
                    val i = Intent(this, Laba3Main::class.java)
                    startActivity(i)
                }
            }
            // close drawer when item is tapped
            drawerLayout.closeDrawers()

            true
        }

        val plAn = JumpAnimation()
        plAn.PlayAnim(jumpAnimCalc)

        jumpAnimCalc.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }
    fun parse(s: EditText) = s.text.toString().toDouble()
    fun sum(a: Int, b: Int) = a + b
    fun raznost(a: Int, b: Int) = a - b
    fun proizvedenie(a: Int, b: Int) = a * b
    fun delenie(a: Double, b: Double) = a / b
}