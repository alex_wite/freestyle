package com.study.lab4

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.study.R
import java.io.Serializable
import java.text.FieldPosition

/*
class Lab4Main : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lab4_main)
    }
}*/

data class CityItem(var cityName: String = "",
                    var cityCode: String = "",
                    var country: String = "") : Serializable

class CityAdapter(private val context: Context,
                  private val dataSource: ArrayList<CityItem>): BaseAdapter(){

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.lab4_main, parent, false)

        val cityNameView = rowView.findViewById<TextView>(R.id.city)
        val cityCodeView = rowView.findViewById<TextView>(R.id.zipCode)

        cityNameView.text = String.format(context.getString(R.string.choose_city_name),
            getItem(position).cityName, getItem(position).country)
        cityCodeView.text = getItem(position).cityCode

        return rowView
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): CityItem {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}